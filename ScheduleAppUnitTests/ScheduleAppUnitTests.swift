//
//  ScheduleAppUnitTests.swift
//  ScheduleAppUnitTests
//
//  Created by Kazybek Ramazan on 12.09.2023.
//

@testable import ScheduleApp
import XCTest

final class ScheduleAppUnitTests: XCTestCase {

    private var presenter: MainViewControllerInput!
    private var service: EventsServiceProtocol!
    
    override func setUp() {
        service = CoreDataMockService()
        
        presenter = MainPresenter(eventsService: service)
        
        presenter.addEvent(.init(title: "Я не курю уже", date: .now))
        presenter.addEvent(.init(title: "Я не пью уже", date: .now))
        presenter.addEvent(.init(title: "Я не хожу уже", date: .now))
    }

    func testInitial() async throws {
        let events = await service.fetchEvents()
        
        XCTAssertTrue(!events.isEmpty, "Ивенты должны быть")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
