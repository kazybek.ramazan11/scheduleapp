//
//  CoreDataMockService.swift
//  ScheduleAppUnitTests
//
//  Created by Kazybek Ramazan on 12.09.2023.
//

@testable import ScheduleApp
import Foundation

final class CoreDataMockService: EventsServiceProtocol {
    
    private var events: [EventModel] = []
    
    func fetchEvents() async -> [EventModel] {
        events
    }
    
    func addEvent(_ model: EventModel) async {
        events.append(model)
    }
    
    func removeEvent(_ model: EventModel) async {
        events.removeAll { event in
            event == model
        }
    }
}
