//
//  TimeModel.swift
//  ScheduleApp
//
//  Created by Ramazan Kazybek on 28.07.2023.
//

struct TimeModel {
    let hour: Int
    let minute: Int
}
