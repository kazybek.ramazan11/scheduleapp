//
//  DayModel.swift
//  ScheduleApp
//
//  Created by Ramazan Kazybek on 28.07.2023.
//

struct DayModel {
    let year: Int
    let month: Int
    let day: Int
}
