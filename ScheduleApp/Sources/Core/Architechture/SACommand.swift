//
//  SACommand.swift
//  ScheduleApp
//
//  Created by Ramazan Kazybek on 27.07.2023.
//

typealias SACommand = () -> Void
